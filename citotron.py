#!/usr/bin/python3
# Wikipedia citation tools
# citotron.py
# Main file

__version__ = '1.0.0'

from args import args
from bs4 import BeautifulSoup as bs
from collections import Counter as counter
from functools import lru_cache
from itertools import repeat
from joblib import Parallel, delayed
from poliplot import *
from pprint import pprint as pprint
from requests import get, post
from resolve import *
from time import sleep
from utils import *
import csv, json, sys, re, os
import resolve
import settings as s

# ---- Random note:
#
#  Patch requests library to do retries:
# 
#  import requests
#  s = requests.Session()
#  a = requests.adapters.HTTPAdapter(max_retries=3)
#  s.mount('http://', a)
        
# ---- HELPERS ----


@lru_cache(maxsize=128)
def pmid_journal_abbrevs(filename=s.PMID_JOURNAL_ABBREVS):
    with open(os.path.join(s.DATADIR, filename),
              mode='rt',
              encoding='utf-8') as f:
        return {canonical(k):canonical(v) for k,v in json.load(f).items()}

    
@lru_cache(maxsize=128)
def journal_abbrevs(filename=s.JOURNAL_ABBREVS):
    with open(os.path.join(s.DATADIR, filename),
              mode='rt',
              encoding='utf-8') as f:
        return {canonical(k):canonical(v) for k,v in json.load(f).items()}


def print_fails():
    print("=" * 80)
    print("Fails by kind:")
    for k,v in kindsfails.items():
        print("    %s: %s" % (k,v))
    print("Fails by resolver:")
    for k,v in resolversfails.items():
        print("    %s: %s" % (k,v))
    print("Processed rows: %s" % total)
    print("Total fails: %s" % fails)
    print("Total successes: %s" % successes)
    print("Fail rate:", str((fails / total) * 100)[:4] + "%")


def safeprint(*args):
    """Convert arguments to strings, False to "ERROR", print the result."""
    print(*[str(arg) if arg is not False else 'ERROR' for arg in args], sep="")


def scientific_publishers():
    """Return a list of scientific publishers read from file."""
    with open(os.path.join(s.DATADIR, s.SCIENTIFIC_PUBLISHERS),
              mode='rt', encoding='utf-8') as f:
        publishers = [l.strip() for l in f.readlines()]
    return publishers


def canonical(title):
    """Bring the title to a canonical form"""
    return re.sub("[^a-zA-z0-9]+", " ", title).strip().upper()


def expand(title):
    """Expand abbreviations in title."""
    if title in journal_abbrevs().keys():
        return journal_abbrevs()[title].strip()
    if title in pmid_journal_abbrevs().keys():
        return  pmid_journal_abbrevs()[title].strip()
    title += ' '
    return title.strip()

# ---- GENERAL FUNCTIONS ----

def resolve_row(row, serial, n=0):
    '''
    Resolves title and publisher for row, where uid = row['id'] and kind = row['type'].
    - Calls resolver function resolve.<kind><n>.
    - If resolver returns string as first element, it returns the result.
    - Else, if resolver returns False as first element, tries resolve.<kind><n+1>, etc.
    - Finally, if resolver does not exist, it returns False, False.
    '''
    global total, successes, fails
    resolver = row['type'] + str(n)
    try:
        resolved = globals()[resolver](row['id'])
        safeprint("-" * 80)
        safeprint("[", str((serial / total) * 100)[:4],"%] ",
                  serial, "/", total,
                  " (", successes, " successes / ", fails, " fails) ",
                  str((fails / serial) * 100)[:4],"% error rate."
                  " Cc: ", resolver, ":", "\n",
                  "    ", resolved[0], ", [", resolved[1], "]")
        sleep(args.sleep)
        if resolved[0]:
            resolved = expand(canonical(resolved[0])), canonical(resolved[1])
            log.writerow([resolved[0]])
            print("   ", resolved[0] + ", [" + resolved[1] + "]")
            successes += 1
            return resolved
        else:
            resolversfails[resolver] += 1
        return resolve_row(row, serial, n + 1)
    except KeyError:
        fails += 1
        kindsfails[row['type']] += 1
        return False, False


def resolve_rows(rows, sci=False):
    """Resolve rows to titles in parallel."""
    n = 8 if sci else 0
    return (Parallel(n_jobs=args.jobs,
                     backend='threading')
            (delayed(resolve_row)(row, serial, n) for row, serial, n in
             zip(rows, range(1, len(rows)), repeat(n))))
    
# ---- MISC. FUNCTIONS ----

def isbns_in_libthing(rows):
    '''
    Tells how many ISBNS of rows are in Library Thing.
    '''
    # List of all ISBNs in Library Thing:
    # http://www.librarything.com/feeds/AllLibraryThingISBNs.csv
    libthing = open(os.path.join(s.DATADIR,
                                 'AllLibraryThingISBNs.csv'), 'r').readlines()
    libthing = [x.rstrip() for x in libthing]
    isbns = [row['id'] for row in rows if row['type'] == 'isbn']
    hits = 0
    for isbn in isbns:
        if isbn in libthing:
            hits += 1
    print("We have %s ISBNS and only %s of them are in Library Thing." % (str(len(isbns)), str(hits)))
    return hits


def count_titles(titles):
    '''
    Accepts a list of titles, logging and returning their frequencies.
    '''
    freq = counter(titles).most_common()
    for k, v in freq:
        out.writerow([k, v])
    return freq


def count_types(rows):
    """Counts how many rows of each kind (isbn, doi, etc.) are in the input csv."""
    types = []
    for row in rows:
        types.append(row['type'])
    return counter(types).most_common()


def filter_kind(kind, rows):
    return [row for row in rows if row['type'] == kind]


def scisbn(rows):
    """Returns statistics about which ISBNs are scientific"""
    global successes, fails
    rows = newrows
    resolve_rows(rows, sci=True)
    return {'All ISBNs': len(rows),
            'Scientific ISBNs': successes,
            'Non-scientific ISBNs': fails}

# ---- INIT ----

# Enumerate generator so that we only read the file once
rows = list(csv.DictReader(open(os.path.join(s.DATADIR, args.inputfile), 'rt'),
                           delimiter="\t"))
log = csv.writer(open(s.LOGFILE, 'w', encoding='utf-8'),
                 delimiter='|',
                 quotechar='"',
                 quoting=csv.QUOTE_MINIMAL)
out = csv.writer(open(args.outputfile, 'w', encoding='utf-8'),
                 delimiter='|',
                 quotechar='"',
                 quoting=csv.QUOTE_MINIMAL)


# --- NORMALISE ----

# Some ISBNs end with small x, which is against the ISBN specification:
newrows = []
count = 0
for row in rows:
    if row['type'] == 'isbn' and 'x' in row['id']:
        row['id'] = row['id'].replace('x', 'X')
        count += 1
    newrows.append(row)
rows = newrows
print("**** --- Corrected lowercase 'x' character in " + str(count) + " ISBNs --- ****")
    
newrows = [row for row in rows if row['type'] != 'isbn' or (row['type'] == 'isbn' and is_valid_isbn(row['id']))]
count = len(rows) - len(newrows)
print("**** ---- Dropped " + str(count) + " invalid ISBNs ---- ****")
rows = newrows

# ---- GLOBAL STATE ----

total, fails, successes = len(rows), 0, 0
kinds = [x[:-1] for x in dir(resolve) if '0' in x]
kindsfails = {k:v for k, v in zip(kinds, repeat(0))}

resolvers = dir(resolve)
resolversfails = {k:v for k, v in zip(resolvers, repeat(0))}


# ---- CALLS ----

if args.kind != 'all':
    rows = filter_kind(args.kind, rows)
    total = len(rows)

m = args.mode
if m == "count":
    pprint(count_titles(resolve_rows(rows)))
    print_fails()
    plot_occurrences(args.outputfile, args.discriminant)
elif m == "resolve":
    resolve_rows(rows)
    print_fails()
elif m == "types":
    pprint(count_types(rows))
elif m == "scisbn":
    pprint(scisbn(filter_kind('isbn', rows)))
else:
    print("mode was: '" + str(m) + "'")
    print("="*80)
    print("You have to choose a supported mode!")
    print("Try to run the script without arguments to get a help message.")
