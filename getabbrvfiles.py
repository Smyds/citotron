import ftputil
import urllib.request
import tarfile

print('getting text file from ftp.ncbi.nih.gov')
with ftputil.FTPHost('ftp.ncbi.nih.gov', 'anonymous', 'citotron@example.com') as host:
	host.download('pubmed/J_Entrez.txt', 'data/J_Entrez.txt')

print('getting tar file from')
print('http://www.compholio.com/latex/jabbrv/downloads/jabbrv_2014-01-21.tar.gz')
urllib.request.urlretrieve('http://www.compholio.com/latex/jabbrv/downloads/jabbrv_2014-01-21.tar.gz', 'data/jabbrv_2014-01-21.tar.gz')

tar = tarfile.open("data/jabbrv_2014-01-21.tar.gz")
tar.extractall(path='data/')


