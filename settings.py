# Citotron settings

DATADIR = "data"
LOGFILE = "log.csv"
SCIENTIFIC_PUBLISHERS = "scientific_publishers.lst"
JOURNAL_ABBREVS = "journal_abbrevs.json"
PMID_JOURNAL_ABBREVS = "pmid_journal_abbrevs.json"
